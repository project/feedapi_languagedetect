<?php
/**
 * Implementation of feed element mapper for language.module.
 */

/**
 * Implementation of hook_feedapi_mapper().
 * Maps feed elements to language fields.
 * @param $op
 *   'list' or 'map'
 * @param $node
 *   Feed item node to map on.
 * @param $field_name
 *   Name of field to map to.
 * @param $feed_element
 *   Feed item element to map from. Parameter only present on $op = 'map'
 * @param @sub_field
 *   Subfield on field to map to. Parameter only present on $op = 'map'.
 *   This parameter will depend on if the hook implementation returns a subfield on 
 *   $op = 'list'.
 *
 * Return key => name array of sub fields on 'list'.
 * If only one field and no subfields are available for this content type return TRUE.
 * If no fields available for this content type return FALSE.
 * Options are necessary because a field like
 * e. g. "taxonomy" might have more than one slot for information - e. g. vocabularies.
 * 
 * Todo: $node could be passed by reference - implementers wouldn't need to return node
 * (PHP5 passes in by reference by default).
 */
function feedapi_languagedetect_feedapi_mapper($op, $node, $field_name, $feed_element = array(), $sub_field = '') {
  if ($field_name == 'language') {
	if ($op == 'describe') {
      return t('Maps the language of a feed element to a node.');
    }
    else if ($op == 'list') {
      return "language";
    }
    else if ($op == 'map') {
      if (!is_array($feed_element)) {
        $node->language = $feed_element;
      }
      return $node;
    }
  } else {
    return FALSE;
  }
}
?>