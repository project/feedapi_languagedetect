==================================================================================
Basic usage
==================================================================================

Enable the feedapi_languagedetect module. The language detect modules' settings will appear under the feed settings.
You can enable/disable the module for each feed. Please note that language detection will consume time. 